# ATLASPix3.1 Quad-Module Design

This GitLab Repository shows the files of PCI adapter board of the quad module, power and data pig tails of Altium Files and Schematics for further usage.

1) The 3D and 2D pictures are provided related to adapter board and the other equipment in the PNG file.
2) Altium files are located in Altium Files & Schematics trajectory.
3) Some presentations which are ATLASPix3 modules status report and the general structure of the quad module setup are shared in the presentations trajectory.


